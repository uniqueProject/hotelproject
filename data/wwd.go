package data

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"
)

type All struct {
	Users  []User  `json:"users"`
	Admins []Admin `json:"admins"`
	Owners []Owner `json:"owners"`
}

type Room struct {
	Price      uint16    `json:"price"`
	NumberRoom uint16    `json:"number_room"`
	Type       uint8     `json:"type"`
	Status     bool      `json:"status"`
	Persons    uint8     `json:"persons"`
	UserId     string    `json:"user_id"`
	Expire     time.Time `json:"expire"`
}

type Hotel struct {
	Name    string `json:"name"`
	Star    uint8  `json:"star"`
	Id      string `json:"id"`
	Rooms   []Room `json:"rooms"`
	Owner   string `json:"owner"`
	Admin   string `json:"admin"`
	Address string `json:"address"`
	History string `json:"history"`
	Budget  uint   `json:"budget"`
}
type User struct {
	Id       string   `json:"id"`
	Name     string   `json:"name"`
	Lastname string   `json:"lastname"`
	UserName string   `json:"username"`
	Password string   `json:"password"`
	Email    string   `json:"email"`
	Phone    string   `json:"phone"`
	Budget   uint32   `json:"budget"`
	HotelIds []string `json:"hotel_ids"`
}

type Admin struct {
	Id        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	UserName  string `json:"user_name"`
	Phone     string `json:"phone"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

type Owner struct {
	Id        string   `json:"id"`
	FirstName string   `json:"first_name"`
	LastName  string   `json:"last_name"`
	UserName  string   `json:"user_name"`
	Phone     string   `json:"phone"`
	Email     string   `json:"email"`
	Password  string   `json:"password"`
	Hotels    []string `json:"hotels"`
}

func GarbageCollector() {
	hotels := ReadFromHotel()
	users := ReadFromAll()
	userId := ""
	a := 0
	for inx, hotel := range hotels {
		for index, room := range hotel.Rooms {
			if time.Now().After(room.Expire) {
				a++
				hotels[inx].Rooms[index].Status = true
				userId = room.UserId
				hotels[inx].Rooms[index].UserId = ""
				s := 0
				for _, rum := range hotels[inx].Rooms {
					if rum.UserId == userId {
						s = 1
						break
					}
				}
				if s == 0 {
					for ind, user := range users.Users {
						if user.Id == room.UserId {
							for in, hIds := range user.HotelIds {
								if hIds == hotel.Id {
									if in != len(user.HotelIds)-1 {
										users.Users[ind].HotelIds = append(users.Users[ind].HotelIds[:in], users.Users[ind].HotelIds[in+1:]...)
									} else {
										users.Users[ind].HotelIds = append(users.Users[ind].HotelIds[:in])
									}
								}
							}
						}
					}
					break
				}
			}
		}
	}
	if a != 0 {
		WriteToHotel(hotels)
		WriteToAll(users)
	}
}

func EmailSearch(email string) bool {
	ls := ReadFromAll()
	for _, v := range ls.Users {
		if v.Email == email {
			return true
		}
	}
	for _, v := range ls.Admins {
		if v.Email == email {
			return true
		}
	}
	for _, v := range ls.Owners {
		if v.Email == email {
			return true
		}
	}
	return false
}

func UsernameSearch(username string) bool {
	ls := ReadFromAll()
	for _, v := range ls.Users {
		if v.UserName == username {
			return true
		}
	}
	for _, v := range ls.Admins {
		if v.UserName == username {
			return true
		}
	}
	for _, v := range ls.Owners {
		if v.UserName == username {
			return true
		}
	}
	return false
}

func EmailCheck(email string) bool {
	c1 := 0
	c2 := 0
	rightemail := ""
	result := 0
	if EmailSearch(email) {
		return false
	}
	for i := 0; i < len(email); i++ {
		if email[i] == 32 {
			return false
		}
		if string(email[i]) == "@" {
			c1 = i
		}
		if c1 < i && c1 != 0 {
			rightemail += string(email[i])
			c2 += 1
		} //@gmail.com || @mail.ru
		if c2 == 9 && i == len(email)-1 {
			if rightemail == "gmail.com" {
				result = 1
			}
		}
		if c2 == 8 && i == len(email)-1 {
			if rightemail == "mail.ru" {
				result = 1
			}
		}
	}
	if result == 1 {
		return true
	}
	return false
}

func PasswordCheck(password string) bool {
	s, f := 0, []int{0, 0, 0}
	if len(password) > 6 {
		for _, v := range password {
			if v == 32 {
				s = 1
			} else if v >= 65 && v <= 90 {
				f[0] = 1
			} else if v >= 97 && v <= 122 {
				f[1] = 2
			} else if v >= 48 && v <= 57 {
				f[2] = 3
			}
		}
		if s == 0 && f[0]+f[1]+f[2] == 6 {
			return true
		}
		fmt.Println("You should use at least 1 capital letter, digit, lowercase letter in your password and the length should be longer than 6")
		return false
	}
	fmt.Println("You should use at least 1 capital letter, digit, lowercase letter in your password and the length should be longer than 6")
	return false
}

func WriteToHotel(hotels []Hotel) {
	b, er := json.MarshalIndent(hotels, "", "  ")
	if er != nil {
		fmt.Println("Error while marshalling b")
		log.Fatal()
	}
	if er = os.WriteFile("data/hotel.json", b, 0644); er != nil {
		fmt.Println("Error while writing to hotel.json")
		log.Fatal()
	}
}

func ReadFromHotel() []Hotel {
	var hotels []Hotel
	b, er := os.ReadFile("data/hotel.json")
	if er != nil {
		fmt.Println("Error while reading file hotel.json")
		log.Fatal()
	}
	if er = json.Unmarshal(b, &hotels); er != nil {
		fmt.Println("Error while unmarshalling b")
		log.Fatal()
	}
	return hotels
}

func WriteToAll(all All) {
	b, er := json.MarshalIndent(all, "", "  ")
	if er != nil {
		fmt.Println("Error while marshalling b")
		log.Fatal()
	}
	if er = os.WriteFile("data/all.json", b, 0644); er != nil {
		fmt.Println("Error while writing to all.json")
		log.Fatal()
	}
}

func ReadFromAll() All {
	var all All
	b, er := os.ReadFile("data/all.json")
	if er != nil {
		fmt.Println("Error while reading file all.json")
		log.Fatal()
	}
	if er = json.Unmarshal(b, &all); er != nil {
		fmt.Println("Error while unmarshalling b")
		log.Fatal()
	}
	return all
}
