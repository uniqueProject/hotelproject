package admin

import (
	"fmt"
	"hotel/data"
	"os"
	"os/exec"
	"sort"
	"time"
)

var (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
)

type Admin data.Admin

func (a Admin) adminhotel() int {
	for i, v := range data.ReadFromHotel() {
		if v.Admin == a.Id {
			return i
		}
	}
	return 0
}

func (a *Admin) NewWrite() {
	all := data.ReadFromAll()
	user := data.Admin{
		Id:        a.Id,
		FirstName: a.FirstName,
		LastName:  a.LastName,
		UserName:  a.UserName,
		Phone:     a.Phone,
		Email:     a.Email,
		Password:  a.Password,
	}
	all.Admins = append(all.Admins, user)
	data.WriteToAll(all)
}

func (a Admin) Delete(num uint16) {
	hotel := data.ReadFromHotel()
	index := a.adminhotel()
	sum := 0
	var userId string
	for i, val := range hotel[index].Rooms {
		if val.NumberRoom == num {
			if val.Status {
				return
			}
			hotel[index].Rooms[i].Status = true
			userId = hotel[index].Rooms[i].UserId
			hotel[index].Rooms[i].UserId = ""
		}
	}
	for i, _ := range hotel[index].Rooms {
		if hotel[index].Rooms[i].UserId == userId {
			sum++
		}
	}
	if sum == 0 {
		all := data.ReadFromAll()
		for i, _ := range all.Users {
			if all.Users[i].Id == userId {
				b := len(all.Users[i].HotelIds)
				for in, val := range all.Users[i].HotelIds {
					if val == hotel[i].Id {
						if b-1 == in {
							all.Users[i].HotelIds = all.Users[i].HotelIds[:b-2]
						} else {
							all.Users[i].HotelIds = append(all.Users[i].HotelIds[:in], all.Users[i].HotelIds[in+1:]...)
						}
					}
				}
			}
		}
		data.WriteToAll(all)
	}
	data.WriteToHotel(hotel)
}

func (a Admin) Rooms() {
	persons := make([][]data.Room, 12)
	listOfHotels := data.ReadFromHotel()
	b := a.adminhotel()
	clear()
	for _, room := range listOfHotels[b].Rooms {
		if room.Type == 1 {
			persons[0+room.Persons-1] = append(persons[0+room.Persons-1], room)
		} else if room.Type == 2 {
			persons[4+room.Persons-1] = append(persons[4+room.Persons-1], room)
		} else if room.Type == 3 {
			persons[8+room.Persons-1] = append(persons[8+room.Persons-1], room)
		}
	}
	for y := range persons {
		sort.Slice(persons[y], func(a, b int) bool {
			return persons[y][b].NumberRoom > persons[y][a].NumberRoom
		})
	}
	printRooms(persons)
}

func printRooms(persons [][]data.Room) {
	for y := range persons {
		if y < 4 {
			if len(persons[y]) != 0 {
				fmt.Printf("Lux room to %d person(s) costs %d\n", y+1, persons[y][0].Price)
			}
			for i, x := range persons[y] {
				if i%10 == 0 {
					fmt.Println()
				}
				if x.Status {
					fmt.Printf("| %s%d%s | ", Green, x.NumberRoom, Reset)
				} else {
					fmt.Printf("| %s%d%s | ", Red, x.NumberRoom, Reset)
				}
			}
		} else if y < 8 && y > 3 {
			if len(persons[y]) != 0 {
				fmt.Printf("\nPremium room to %d person(s) costs %d\n", y-3, persons[y][0].Price)
			}
			for i, x := range persons[y] {
				if i%10 == 0 {
					fmt.Println()
				}
				if x.Status {
					fmt.Printf("| %s%d%s | ", Green, x.NumberRoom, Reset)
				} else {
					fmt.Printf("| %s%d%s | ", Red, x.NumberRoom, Reset)
				}
			}
		} else if y < 12 && y > 7 {
			if len(persons[y]) != 0 {
				fmt.Printf("\nOrdinary room to %d person(s) costs %d\n", y-7, persons[y][0].Price)
			}
			for i, x := range persons[y] {
				if i%10 == 0 {
					fmt.Println()
				}
				if x.Status {
					fmt.Printf("| %s%d%s | ", Green, x.NumberRoom, Reset)
				} else {
					fmt.Printf("| %s%d%s | ", Red, x.NumberRoom, Reset)
				}
			}
		}
	}
	fmt.Println()
}

func (a *Admin) Booking(u data.User, numOfRoom uint16, day int) (bool, bool) {
	listOfHotels := data.ReadFromHotel()
	i := a.adminhotel()
	for in, room := range listOfHotels[i].Rooms {
		if room.NumberRoom == numOfRoom {
			if !room.Status {
				return false, true
			}
			if u.Budget > uint32(room.Price)*uint32(day) {
				u.Budget -= uint32(room.Price) * uint32(day)
				listOfHotels[i].Rooms[in].Status = false
				listOfHotels[i].Rooms[in].UserId = u.Id
				u.HotelIds = append(u.HotelIds, listOfHotels[i].Id)
				listOfHotels[i].Budget += uint(room.Price)
				listOfHotels[i].Rooms[in].Expire = time.Now().Add(time.Hour * 24 * time.Duration(day))
				y, m, d := time.Now().Date()
				h, min, _ := time.Now().Clock()
				listOfHotels[i].History += fmt.Sprintf("%d %s %d %d:%d: %s %s ordered the room №%d for %d day(s)\n", y, m, d, h, min, u.Name, u.Lastname, room.NumberRoom, day)
				data.WriteToHotel(listOfHotels)
				updateUser(u.Id, u.Budget, u.HotelIds)
				return true, true
			} else {
				fmt.Println("You dont have enough money!")
				return false, false
			}
		}
	}
	return true, false
}

func updateUser(Id string, Budget uint32, HotelId []string) {
	all := data.ReadFromAll()
	for i, x := range all.Users {
		if x.Id == Id {
			all.Users[i].Budget = Budget
			all.Users[i].HotelIds = HotelId
		}
	}
	data.WriteToAll(all)
}

func clear() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
