package start

import (
	"bufio"
	"fmt"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"hotel/app"
	"hotel/data"
	"os"
	"os/exec"
	"strconv"
)

const (
	menu              = iota //0
	signIn                   //1
	signUp                   //2
	exit                     //3
	userRegistration         //4
	ownerRegistration        //5
	userMenu                 //6
	adminMenu                //7
	ownerMenu                //8
	userBroone               //9
	userRoomList             //10
	userProfil               //11
	adminBroone              //12
	deleteBroon              //13
	myHotels                 //14
	hotelAdd                 //15
	deleteHotel              //16
	ownerProfile             //17
	adminRegistration        //18
	roomRegistration         //19
)

var (
	adminoff   bool
	comingBack = 1
)

func Run() {
	appList := app.App{}
	var answer1 int
	var loginUsrname, loginpassword string
	fmt.Println("			NION")
	fmt.Printf("Sign Up to book, add, manage hotels remote\n" +
		"	1 - Login\n" +
		"	2 - Sign Up\n" +
		"	3 - Exit\n")
	answer1 = CorrectInput("Your choice: ", 1, 3)
	for {
		switch answer1 {
		case menu:
			clear()
			fmt.Printf("Sign Up to book, add, manage hotels remote\n" +
				"	1 - Login\n" +
				"	2 - Sign Up\n" +
				"	3 - Exit\n")
			answer1 = CorrectInput("Your choice: ", 1, 3)
		case signIn:
			clear()
			fmt.Println("Please enter your")
		login:
			loginUsrname = rightInput("Username (0 - exit): ")
			if loginUsrname == "0" {
				answer1 = 0
				continue
			}
			for {
				if loginpassword = rightInput("Password (0 - exit): "); loginpassword == "0" || data.PasswordCheck(loginpassword) {
					break
				}
			}
			if loginpassword == "0" {
				answer1 = 0
				continue
			}
			role, usrexists, corPass := appList.SingIn(loginUsrname, loginpassword)
			if !usrexists {
				fmt.Println("Username not found try again")
				goto login
			} else if !corPass {
				fmt.Println("Password is not correct try again ")
				goto login
			} else {
				if role == 1 && adminoff {
					answer1 = 12
					continue
				}
				answer1 = role + 5
			}
		case signUp:
			clear()
			fmt.Printf("	1 - User registration\n" +
				"	2 - Owner registration\n")
			answer1 = CorrectInput("Your choice: ", 1, 2)
			answer1 += 3
		case userRegistration:
			appList.NewUser()
			appList.User.Name = rightInput("Name: ")
			appList.User.Lastname = rightInput("Last name: ")
			for {
				if email := rightInput("Email: "); data.EmailCheck(email) {
					appList.User.Email = email
					break
				}
			}
			appList.User.Phone = rightInput("Phone number: ")
			for {
				if usrname := rightInput("Username: "); !data.UsernameSearch(usrname) {
					appList.User.UserName = usrname
					break
				}
			}
			for {
				if pass := rightInput("Password: "); data.PasswordCheck(pass) {
					appList.User.Password, _ = HashPassword(pass)
					break
				}
			}
			appList.User.Budget = rightInt("Your budget: ")
			appList.User.Id = uuid.New().String()
			appList.User.NewWrite()
			if adminoff {
				answer1 = 12
				continue
			}
			answer1 = 1

		case ownerRegistration:
			clear()
			appList.NewOwner()
			appList.Owner.Id = uuid.New().String()
			appList.Owner.FirstName = rightInput("Name: ")
			appList.Owner.LastName = rightInput("Last name: ")
			for {
				if email := rightInput("Email: "); data.EmailCheck(email) {
					appList.Owner.Email = email
					break
				}
			}
			appList.Owner.Phone = rightInput("Phone number: ")
			for {
				if usrname := rightInput("Username: "); !data.UsernameSearch(usrname) {
					appList.Owner.UserName = usrname
					break
				}
			}
			for {
				if pass := rightInput("Password: "); data.PasswordCheck(pass) {
					appList.Owner.Password, _ = HashPassword(pass)
					break
				}
			}
			appList.Owner.NewOwner()
			answer1 = 1 //o'zgartirishda....
		case userMenu:
			clear()
			fmt.Printf("	1 - Bron room\n" +
				"	2 - User's room list\n" +
				"	3 - User's profile\n" +
				"	0 - Exit\n")
			answer1 = CorrectInput("your choice: ", 0, 3)
			if answer1 == 0 {
				answer1 = 0
				continue
			}
			answer1 += 8
		case userBroone:
			clear()
			appList.Garbage()
			appList.User.List()
			hotelnum := askingNum("Choice hotel number ( 0 - back ): ")
			if hotelnum == 0 {
				answer1 = 6
				continue
			}
			appList.User.Rooms(hotelnum)
		roomchoice:
			roomnum := askingNum("Choice room number ( 0 - back ): ")
			if roomnum == 0 {
				answer1 = 9
				continue
			}
			stayingdays := askingNum("How many days you stay: ")
			if free, exists := appList.User.Booking(uint16(roomnum), hotelnum, stayingdays); !exists && free {
				fmt.Println("This room does not exist")
				goto roomchoice
			} else if !free && exists {
				fmt.Println("This room is not free ")
				goto roomchoice
			} else if !free && !exists {
				fmt.Println("You don't have enough money")
				goto roomchoice
			}
			answer1 = 6
		case userRoomList:
			appList.Garbage()
			clear()
			appList.User.ShowBooking()
			answer := askingNum("\n(0 - exit): ")
			if answer != 0 {
				answer1 = 10
				continue
			}
			answer1 = 6
		case userProfil:
			clear()
			appList.User.UserProfile()
			choice := askingNum("	1 - changing username\n" +
				"	2 - changing password\n" +
				"	0 - Exit\n" +
				"	your choice: ")
			if choice == 1 {
				for {
					if appList.User.UserName == rightInput("Old username: ") {
						for {
							if newusrname := rightInput("New username: "); !data.UsernameSearch(newusrname) {
								appList.User.ChangeUsername(newusrname)
								break
							} else {
								fmt.Println("This username is already taken")
							}
						}
						break
					} else {
						fmt.Println("Incorrect username ")
					}
				}
			} else if choice == 2 {
				for {
					if CheckPasswordHash(rightInput("Old password: "), appList.User.Password) {
						for {
							if newpassword := rightInput("New password: "); data.PasswordCheck(newpassword) {
								newpass, _ := HashPassword(newpassword)
								appList.User.ChangePassword(newpass)
								break
							} else {
								fmt.Println("Invalid password")
							}
						}
						break
					} else {
						fmt.Println("Incorrect password ")
					}
				}
			} else if choice == 0 {
				answer1 = 6
				continue
			} else {
				answer1 = 11
				continue
			}
		case adminMenu:
			clear()
			fmt.Printf("	1 - Bron\n" +
				"	2 - Delete bron\n" +
				"	0 - Exit\n")
			choice := CorrectInput("Your choice: ", 0, 2)
			if choice == 1 {
				answer1 = 12
				continue
			} else if choice == 2 {
				answer1 = 13
				continue
			} else if choice == 0 {
				answer1 = 0
			}
		case adminBroone:
			appList.Garbage()
			clear()
			if !adminoff {
				answ := askingNum("Do you have an account? ( 1 - yes / 2 - no / 0 - exit): ")
				if answ == 1 {
					adminoff = true
					answer1 = 1
					continue
				} else if answ == 2 {
					adminoff = true
					answer1 = 4
					continue
				} else if answ == 0 {
					answer1 = 7
					continue
				} else {
					answer1 = 12
					continue
				}
			}
			adminoff = false
			appList.Admin.Rooms()
		roomnumchoice:
			roomnum := askingNum("Enter room number ( exit - 0 ): ")
			if roomnum == 0 {
				answer1 = 7
				continue
			}
			stayingday := askingNum("Days you staying: ")
			appList.Admin.Booking(appList.DataUser(), uint16(roomnum), stayingday)
			if free, exists := appList.Admin.Booking(appList.DataUser(), uint16(roomnum), stayingday); !exists && free {
				fmt.Println("This room is not exists")
				goto roomnumchoice
			} else if !free && exists {
				fmt.Println("This room is not free ")
				goto roomnumchoice
			} else if !free && !exists {
				fmt.Println("You don't have enough money")
				goto roomnumchoice
			}
			answer1 = 7
		case deleteBroon:
			appList.Garbage()
			clear()
			appList.Admin.Rooms()
			roomNum := askingNum("Enter room number (0 - exit): ")
			appList.Admin.Delete(uint16(roomNum))
			answer1 = 7
		case ownerMenu:
			clear()
			fmt.Printf("	1 - My hotels\n" +
				"	2 - Add hotel\n" +
				"	3 - Delete hotel\n" +
				"	4 - View profile\n" +
				"	0 - Exit\n")
			choice := CorrectInput("Your choice: ", 0, 4)
			if choice == 0 {
				answer1 = 0
				continue
			} else {
				answer1 = choice + 13
				continue
			}
		case myHotels:
			clear()
			appList.Owner.MyHotels()
			choice := 0
			for {
				choice = askingNum("Enter hotel number ( Exit - 0 ): ")
				if choice == 0 {
					answer1 = 8
					break
				}
				if !appList.Owner.ViewHotel(choice) {
					break
				}
			}
			if choice == 0 {
				continue
			}
			choice = askingNum("Back - 0: ")
			if choice == 0 {
				answer1 = 14
				continue
			}

		case hotelAdd:
			clear()
			if comingBack == 1 {
				appList.NewHotel()
				appList.Hotel.Name = rightInput("Hotel's name: ")
				appList.Hotel.Star = uint8(CorrectInput("Hotel's star rate: ", 1, 5))
				appList.Hotel.Id = uuid.New().String()
				appList.Hotel.Address = rightInput("Hotel's address: ")
				appList.Hotel.Owner = appList.Owner.Id
				comingBack = 2
				answer1 = 19
				continue
			} else if comingBack == 2 {
				appList.Hotel.Budget = uint(askingNum("Hotel's budget: "))
				answer1 = 18
				comingBack = 3
				continue
			} else {
				appList.Hotel.Admin = appList.Admin.Id
				appList.Hotel.Write()
				appList.Owner.Hotels = append(appList.Owner.Hotels, appList.Hotel.Id)
				appList.Owner.NewOwner()
				comingBack = 1
				answer1 = 8
				continue
			}
		case roomRegistration:
			clear()
			if CorrectInput("Do you have Lux type room for 1 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(1, 1, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Lux type room for 2 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(1, 2, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Lux type room for 3 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(1, 3, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Lux type room for 4 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(1, 4, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Premium type room for 1 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(2, 1, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Premium type room for 2 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(2, 2, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Premium type room for 3 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(2, 3, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Premium type room for 4 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(2, 4, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Ordinary type room for 1 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(3, 1, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Ordinary type room for 2 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(3, 2, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Ordinary type room for 3 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(3, 3, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}

			if CorrectInput("Do you have Ordinary type room for 4 person? (1 <- yes / 2 <- no): ", 1, 2) == 1 {
				money := rightInt("Cost of room? : ")
				fmt.Println("If the number of your rooms are consequence enter like (101 - 107)")
				for {
					ac := rightInput("Enter the number of rooms: ")
					appList.Hotel.AddRum(3, 4, uint16(money), ac)
					aa := CorrectInput("Do you have the same room(s)? (1 <- yes / 2 <- no): ", 1, 2)
					if aa == 2 {
						break
					}
				}
			}
			answer1 = 15
		case adminRegistration:
			clear()
			appList.NewAdmin()
			appList.Admin.FirstName = rightInput("Enter Admin's firstname: ")
			appList.Admin.LastName = rightInput("Enter Admin's lastname: ")
			appList.Admin.Id = uuid.New().String()
			for {
				if username := rightInput("Enter Admin's username: "); !data.UsernameSearch(username) {
					appList.Admin.UserName = username
					break
				}
				fmt.Println("Incorrect input!")
			}

			for {
				if password := rightInput("Enter Admin's password: "); data.PasswordCheck(password) {
					appList.Admin.Password, _ = HashPassword(password)
					break
				}
				fmt.Println("Incorrect input!")
			}

			for {
				if email := rightInput("Enter Admin's email: "); data.EmailSearch(email) {
					appList.Admin.Email = email
					break
				}
				fmt.Println("Incorrect input!")
			}
			appList.Admin.Phone = rightInput("Enter Admin's phone number: ")
			appList.Admin.NewWrite()
			answer1 = 15
		case deleteHotel:
			clear()
			appList.Owner.MyHotels()
			for {
				index := rightInt("Which hotel do you want to delete? (0 - exit) : ")
				if index == 0 {
					break
				}
				if !appList.Owner.DeleteHotel(appList.Owner.Hotels[index-1]) {
					a := rightInt("Enter 0 to exit: ")
					if a == 0 {
						break
					}
				}
			}
			answer1 = 8
		case ownerProfile:
			clear()
			fmt.Println("Name: ", appList.Owner.FirstName)
			fmt.Println("Lastname: ", appList.Owner.LastName)
			fmt.Println("Username: ", appList.Owner.UserName)
			fmt.Print("Password: ")
			fmt.Print("* * * * * *")
			fmt.Println()
			fmt.Println("Phone: ", appList.Owner.Phone)
			fmt.Println("Email: ", appList.Owner.Email)
			appList.Owner.ChangePassword(rightInput("Old password: "), rightInput("New password: "))
			answer1 = 8
		case exit:
			return
		}
	}
}
func askingNum(s string) int {
	var num int
again:
	fmt.Print(s)
	_, err := fmt.Scan(&num)
	if err != nil {
		goto again
	}
	return num
}
func rightInput(s string) string {
	fmt.Print(s)
	line := bufio.NewScanner(os.Stdin)
	if line.Scan() {
		return line.Text()
	}
	return ""
}
func rightInt(s string) uint32 {
again:
	budget := rightInput(s)
	num, err := strconv.Atoi(budget)
	if err != nil {
		goto again
	}
	return uint32(num)
}
func CorrectInput(s string, first int, last int) int {
again:
	n := rightInput(s)
	ans, err := strconv.Atoi(n)
	if err != nil {
		fmt.Print("invalid choice try again ")
		goto again
	}
	if ans >= first && ans <= last {
		return ans
	} else {
		fmt.Println("incorrect input try again")
		goto again
	}
}
func clear() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
