package app

import (
	"golang.org/x/crypto/bcrypt"
	"hotel/admin"
	"hotel/data"
	"hotel/hotel"
	"hotel/owner"
	"hotel/user"
	"sync"
)

type App struct {
	User  user.User
	Admin admin.Admin
	Owner owner.Owner
	Hotel hotel.Hotel
}

func (a App) Garbage() {
	data.GarbageCollector()
}

func (a *App) NewUser() {
	a.User = user.User{}
}
func (a *App) NewOwner() {
	a.Owner = owner.Owner{}
}
func (a *App) NewAdmin() {
	a.Admin = admin.Admin{}
}
func (a *App) NewHotel() {
	a.Hotel = hotel.Hotel{}
}
func (a *App) DataAdmin() data.Admin {
	return data.Admin{
		Id:        a.Admin.Id,
		FirstName: a.Admin.FirstName,
		LastName:  a.Admin.LastName,
		UserName:  a.Admin.UserName,
		Phone:     a.Admin.Phone,
		Email:     a.Admin.Email,
		Password:  a.Admin.Password,
	}
}
func (a App) DataUser() data.User {
	return data.User{
		Id:       a.User.Id,
		Name:     a.User.Name,
		Lastname: a.User.Lastname,
		UserName: a.User.UserName,
		Password: a.User.Password,
		Email:    a.User.Email,
		Phone:    a.User.Phone,
		Budget:   a.User.Budget,
		HotelIds: a.User.HotelIds,
	}
}
func (a *App) SingIn(name, password string) (int, bool, bool) {
	all := data.ReadFromAll()
	u := 0
	us := false
	var w sync.WaitGroup
	w.Add(3)
	go func() {
		defer w.Done()
		for _, v := range all.Users {
			if v.UserName == name {
				us = true
				if CheckPasswordHash(password, v.Password) {
					a.User = user.User{v.Id, v.Name, v.Lastname, v.UserName, v.Password, v.Email, v.Phone, v.Budget, v.HotelIds}
					u = 1
				}
			}
		}
	}()
	go func() {
		defer w.Done()
		for _, v := range all.Admins {
			if v.UserName == name {
				us = true
				if CheckPasswordHash(password, v.Password) {
					a.Admin = admin.Admin{v.Id, v.FirstName, v.LastName, v.UserName, v.Phone, v.Email, v.Password}
					u = 2
				}
			}
		}
	}()
	go func() {
		defer w.Done()
		for _, v := range all.Owners {
			if v.UserName == name {
				us = true
				if CheckPasswordHash(password, v.Password) {
					a.Owner = owner.Owner{v.Id, v.FirstName, v.LastName, v.UserName, v.Phone, v.Email, v.Password, v.Hotels}
					u = 3
				}
			}
		}
	}()
	w.Wait()
	if !us {
		return 0, false, false
	} else {
		if u == 0 {
			return 0, true, false
		}
	}
	return u, true, true
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
