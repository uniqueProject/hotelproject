package hotel

import (
	"hotel/data"
	"strconv"
	"strings"
	"time"
)

type Hotel data.Hotel

func (h *Hotel) AddRum(tip, persons uint8, price uint16, num string) bool {
	a := ""
	for i, _ := range num {
		if string(num[i]) == "-" {
			if string(num[i-1]) != " " {
				a += " "
			}
			a += string(num[i])
			if string(num[i+1]) != " " {
				a += " "
			}
		} else {
			a += string(num[i])
		}
	}
	b := strings.Split(a, " - ")
	if len(b) > 2 || persons > 4 || persons == 0 || tip > 3 || tip == 0 {
		return false
	}
	n1, err := strconv.Atoi(b[0])
	var n2 int
	if err != nil {
		return false
	}
	if len(b) > 1 {
		n2, err = strconv.Atoi(b[1])
		if err != nil {
			return false
		}
	} else {
		n2 = n1
	}

	if n1 > n2 {
		print("1")

		return false
	}
	for i := n1; i < n2+1; i++ {
		for _, v := range h.Rooms {
			if v.NumberRoom == uint16(i) {
				return false
			}
		}
	}
	for i := n1; i < n2+1; i++ {
		r := data.Room{
			Type:       tip,
			Persons:    persons,
			Price:      price,
			Status:     true,
			NumberRoom: uint16(i),
			UserId:     "",
			Expire:     time.Now(),
		}
		h.Rooms = append(h.Rooms, r)
	}
	return true
}

func (h *Hotel) Write() {
	hotels := data.ReadFromHotel()
	h.History = ""
	hotel := data.Hotel{
		Name:    h.Name,
		Star:    h.Star,
		Id:      h.Id,
		Rooms:   h.Rooms,
		Owner:   h.Owner,
		Admin:   h.Admin,
		Address: h.Address,
		History: h.History,
		Budget:  h.Budget,
	}
	hotels = append(hotels, hotel)
	data.WriteToHotel(hotels)
}
