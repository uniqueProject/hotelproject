package owner

import (
	"fmt"
	"hotel/data"
	"os"
	"text/tabwriter"
)

var (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
)

type Owner data.Owner

func (o Owner) ViewHotel(hotelNum int) bool {
	hotelList := o.Hotels
	hotelNum -= 1
	hotels := data.ReadFromHotel()
	H := data.Hotel{}
	if len(o.Hotels) < hotelNum {
		return false
	}
	for _, val := range hotels {
		if val.Id == hotelList[hotelNum] {
			H = val
		}
	}
	stars := ""
	for i := 0; i < int(H.Star); i++ {
		stars += "* "
	}
	all := 0
	ord := 0
	pre := 0
	lux := 0
	free := 0
	for i := 0; i < len(H.Rooms); i++ {
		if H.Rooms[i].Type == 3 {
			ord += 1
		} else if H.Rooms[i].Type == 2 {
			pre += 1
		} else if H.Rooms[i].Type == 1 {
			lux += 1
		}
		if H.Rooms[i].Status == true {
			free += 1
		}
		all += 1
	}
	fmt.Printf("Hotel Status: %s\n"+
		"Hotel Id: %s\n"+
		"Hotel Address: %s\n"+
		"Hotel Admin: %s\n"+
		"Hotel Rooms: %d from %d lux rooms, %d premium rooms and %d ordinary rooms\n"+
		"And %d free rooms\n"+
		"Hotel budget: %d \n"+
		"Hotel events: %s\n", stars, H.Id, H.Address, H.Admin, all, lux, pre, ord, free, int(H.Budget), H.History)
	return true
}
func (o Owner) MyHotels() {
	MyHotelList := make([]data.Hotel, 0)
	hotelList := data.ReadFromHotel()

	w := tabwriter.NewWriter(os.Stdout, 1, 4, 1, '\t', tabwriter.AlignRight)
	fmt.Printf("№\tName\t\t\tStatus\t\tAddress\n\n")
	for i, val := range hotelList {
		if val.Owner == o.Id {
			MyHotelList = append(MyHotelList, val)
			star := ""
			for i := uint8(0); i < val.Star; i++ {
				star += "* "
			}
			fmt.Fprintf(w, "%d\t%s%s\t\t%s%s\t\t%s%s\n", i+1, Red, val.Name, Yellow, star, Reset, val.Address)
		}
	}
	w.Flush()
}
func (o *Owner) NewOwner() {
	new1 := data.Owner{
		Id:        o.Id,
		FirstName: o.FirstName,
		LastName:  o.LastName,
		UserName:  o.UserName,
		Phone:     o.Phone,
		Email:     o.Email,
		Password:  o.Password,
		Hotels:    o.Hotels,
	}
	all := data.ReadFromAll()
	all.Owners = append(all.Owners, new1)
	data.WriteToAll(all)
}
func (o Owner) MyProfile() data.Owner {
	ownersList := data.ReadFromAll().Owners
	for _, val := range ownersList {
		if val.Id == o.Id {
			return val
		}
	}
	k := data.Owner{}
	return k
}
func (o *Owner) ChangePassword(oldPassword string, newPassword string) bool {
	if o.Password == oldPassword {
		if data.PasswordCheck(newPassword) {
			return true
		}
	}
	return false
}
func (o Owner) DeleteHotel(HotelId string) bool {
	var adminId string
	HoteList := data.ReadFromHotel()
	AdminList := data.ReadFromAll().Admins
	var NewList []data.Hotel
	var NewAdminsList []data.Admin
	for _, val := range HoteList {
		if val.Id == HotelId {
			adminId = val.Admin
		} else {
			NewList = append(NewList, val)
		}
	}
	for _, val := range AdminList {
		if val.Id == adminId {
			continue
		} else {
			NewAdminsList = append(NewAdminsList, val)
		}
	}
	data.WriteToAll(data.All{Admins: NewAdminsList})
	data.WriteToHotel(NewList)
	return true
}
func (o Owner) AddHotel(Hotelid string) bool {
	all := data.ReadFromAll()
	for ind, val := range all.Owners {
		if val.Id == o.Id {
			all.Owners[ind].Hotels = append(o.Hotels, Hotelid)
		}
	}
	data.WriteToAll(all)
	return true
}
